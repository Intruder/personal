package first;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.Scanner;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class Score {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		double smallBefore=0,smallBase=0,smallTest=0,smallProgram=0,smallAdd=0;
		//small
		File small=new File("small.html");
		Document doc = Jsoup.parse(small,"UTF-8");
		Elements smallAll=doc.getElementsByClass("interaction-row");
		for (int i=0;i<smallAll.size();i++)
		{
			//课前自测
			if (smallAll.get(i).toString().contains("课前自测"))
			{
				Elements smallSpan=smallAll.get(i).getElementsByTag("span");
				for (int j=0;j<smallSpan.size();j++)
				{
					if (smallSpan.get(j).text().contains("经验"))
					{
						Scanner sc=new Scanner(smallSpan.get(j).text());
						smallBefore=smallBase+sc.nextDouble();
						break;
					}
				}
			}
			//small课堂完成
			else if (smallAll.get(i).toString().contains("课堂完成")&&smallAll.get(i).toString().contains("已参与&nbsp;"))
			{
				Elements smallSpan=smallAll.get(i).getElementsByTag("span");
				for (int j=0;j<smallSpan.size();j++)
				{
					if (smallSpan.get(j).text().contains("经验"))
					{
						Scanner sc=new Scanner(smallSpan.get(j).text());
						smallBase=smallBase+sc.nextDouble();
						break;
					}
				}
			}
			//small课堂小测
			else if (smallAll.get(i).toString().contains("课堂小测")&&smallAll.get(i).toString().contains("已参与&nbsp;"))
			{
				Elements smallSpan=smallAll.get(i).getElementsByTag("span");
				for (int j=0;j<smallSpan.size();j++)
				{
					if (smallSpan.get(j).text().contains("经验"))
					{
						Scanner sc=new Scanner(smallSpan.get(j).text());
						smallTest=smallTest+sc.nextDouble();
						break;
					}
				}
			}
			//small编程题
			else if (smallAll.get(i).toString().contains("编程题")&&smallAll.get(i).toString().contains("已参与&nbsp;"))
			{
				Elements smallSpan=smallAll.get(i).getElementsByTag("span");
				for (int j=0;j<smallSpan.size();j++)
				{
					if (smallSpan.get(j).text().contains("经验"))
					{
						Scanner sc=new Scanner(smallSpan.get(j).text());
						smallProgram=smallProgram+sc.nextDouble();
						break;
					}
				}
			}
			//small附加题
			else if (smallAll.get(i).toString().contains("附加题")&&smallAll.get(i).toString().contains("已参与&nbsp;"))
			{
				Elements smallSpan=smallAll.get(i).getElementsByTag("span");
				for (int j=0;j<smallSpan.size();j++)
				{
					if (smallSpan.get(j).text().contains("经验"))
					{
						Scanner sc=new Scanner(smallSpan.get(j).text());
						smallAdd=smallAdd+sc.nextDouble();
						break;
					}
				}
			}
		}
		//All
		File all=new File("all.html");
		Document alldoc = Jsoup.parse(all,"UTF-8");
		Elements allAll=alldoc.getElementsByClass("interaction-row");
		for (int i=0;i<allAll.size();i++)
		{
			//All课前自测
			if (allAll.get(i).toString().contains("课前自测"))
			{
				Elements allSpan=allAll.get(i).getElementsByTag("span");
				for (int j=0;j<allSpan.size();j++)
				{
					if (allSpan.get(j).text().contains("经验"))
					{
						Scanner sc=new Scanner(allSpan.get(j).text());
						smallBefore=smallBefore+sc.nextDouble();
						break;
					}
				}
			}
		}
		//获取配置文件
		Properties pr=new Properties();
		pr.load(new FileInputStream("src/total.properties"));
		double totalBefore=Double.parseDouble(pr.getProperty("before"));
		double totalBase=Double.parseDouble(pr.getProperty("base"));
		double totalTest=Double.parseDouble(pr.getProperty("test"));
		double totalProgram=Double.parseDouble(pr.getProperty("program"));
		double totalAdd=Double.parseDouble(pr.getProperty("add"));
		totalProgram=95;
		totalAdd=90;
		//总成绩
		double FinalScore=(smallBase/totalBase)*100*0.3*0.95+(smallTest/totalTest)*100*0.2+(smallBefore/totalBefore)*100*0.25+(smallProgram/totalProgram)*100*0.1+(smallAdd/totalAdd)*100*0.05+6;
		//输出总成绩
		System.out.println(FinalScore);
	}

}
